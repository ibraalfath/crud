import React, { Component } from 'react';
import logo from './logo.png';
import './App.css';

class App extends Component {
  constructor(){
    super()
    this.state={
      judul: "Pondok IT Students",
      act: 0,
      index: "",
      datas: []
    }
  }

  componentDidMount(){
    this.refs.name.focus();
  }

  fKirim = (e)=>{
    e.preventDefault();
    console.log("test")

    let datas = this.state.datas;
    let name = this.refs.name.value;
    let email = this.refs.email.value;

    if(this.state.act === 0){    //new
      let data = {
        name, email
      }
      datas.push(data);
    }else{                      //edit
      let index = this.state.index;
      datas[index].name = name;
      datas[index].email = email;
    }

    
    this.setState({
      datas : datas,
      act: 0
    })

    
  }

  fHapus = (i)=>{
    let datas = this.state.datas;
    datas.splice(i,1);
    this.setState({
      datas : datas
    });

    // this.refs.myForm.reset()
    this.refs.name.focus()
  }

  fEdit = (i) =>{
    let data = this.state.datas[i];
    this.refs.name.value = data.name;
    this.refs.email.value = data.email;

    this.setState({
      act: 1,
      index: i
    })

    this.refs.name.focus();
  }

  render() {
    let datas = this.state.datas; 
    var myStyle = {
      color : 'white'
    }
    return (
      <div className="App">
        <br/>
        <h1 style={myStyle} className='judul'>{this.state.judul}</h1>
        <br/>
        <div className=" container contener">
          <div className="row">
            <div className="col-md-8 ">
                <table class="table myList kategori">
                  <thead class="thead-dark">
                    <tr>
                      <th scope="col">No</th>
                      <th scope="col">Name</th>
                      <th scope="col">Division</th>
                      <th scope="col">Delete</th>
                      <th scope="col">Edit</th>
                    </tr>
                  </thead>
                  {datas.map((data, i)=>
                  <tbody  key={i}>
                    <tr>
                      <th className='colorr' scope="row">{i+1}</th>
                      <td className='colorr'>{data.name}</td>
                      <td className='colorr'>{data.email}</td>
                      <td><button onClick={()=>this.fHapus(i)} className='btn btn-secondary' >Delete</button></td>
                      <td><button onClick={()=>this.fEdit(i)} className='btn btn-secondary' >Edit</button></td>
                    </tr>
                  </tbody>
                  )}
                </table>
            </div>


            
            <div className=" col-md-4">
              <div className="crud shadow-lg">
                <from ref="myForm" className="myForm">
                  <div className="form-group ">
                    <label for="exampleInputEmail1">Your Name</label>
                    <input type="text" ref="name" placeholder="Write down..." className="form-control"/>
                  </div>
                  <div className="form-group ">
                    <label for="exampleInputEmail1">Your Division</label>
                    <input type="text" ref="email" placeholder="Write down..." className="form-control"/>
                  </div>
                  <br/>
                  <button onClick={(e)=>this.fKirim(e)} className='btn btn-secondary ' >Submit</button>
                  <br/><br/>
                  <img src={logo} className="App-logo" alt="logo" />
                </from>
              </div>
            </div>
            
          </div>
        </div>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
      </div>
    );
  }
}

export default App;
